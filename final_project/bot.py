import telebot
from telebot import types  # для указание типов
import random
import emoji
import schedule
import time
from datetime import date
from threading import Thread

# создаем массив из существительных, которые будем загадывать пользователю в сложном режиме
with open('russian_nouns_f.txt', 'r', encoding='utf-8') as file:
    words_h = file.read()
words_h = words_h.split('\n')

# создаем массив из существительных, которые будем загадывать пользователю в простом режиме
with open('easy_mode_f.txt', 'r', encoding='utf-8') as file:
    words_e = file.read()
words_e = words_e.split('\n')

# создаем бота
bot = telebot.TeleBot('6218513407:AAFQemNN2Jckj35h3eYHuyE4OSWm95QWwNo')

# набор возможных длин слова, которые есть в нашем списке (для простого и сложного режима)
lengths_h = ['5', '6', '7', '8', '9', '10', '11', '12', '13', '14',
             '15', '16', '17', '18', '19', '20']
lengths_e = ['3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14',
             '15', '16']

# создаем словари - базы данных
# в db храним длину слова и слово на каждый день на каждого пользователя
db = {}
# в db_names храним имя каждого пользователя
db_names = {}
# в db_words храним слова, загаданные каждому пользователю
db_words_e = {}  # слова простого режима
db_words_h = {}  # слова сложного режима


# пишем функции для бота (он принимает только текстовые сообщения)
@bot.message_handler(content_types=['text'])
# когда пользователь запускает бота, он присылает ему приветствие, обозначения в игре и спрашивает имя, после этого
# запускается функци get_name
def start(message):
    if message.text == '/start':
        bot.send_message(message.from_user.id, emoji.emojize('Привет! :smiling_face_with_open_hands: '
                                                             'Твоя задача - угадать существительное русского языка.'
                                                             ' Я буду присылать тебе новое слово каждый день!'))
        bot.send_message(message.from_user.id, emoji.emojize('Вот обозначения в нашей игре:\n'
                                                             f':green_square: - буква есть в слове и стоит на '
                                                             f'правильном '
                                                             f'месте \n:yellow_square: - буква есть в слове, но стоит'
                                                             f' не на правильно месте \n:red_square: - буквы нет '
                                                             f'в слове'))
        bot.send_message(message.from_user.id, 'Как к тебе можно обращаться? Напиши свое имя.')
        bot.register_next_step_handler(message, get_name)


# функция для преобразования времени в нужный вид
def tconv(x):
    return time.strftime("%Y-%m-%d", time.localtime(x))


# в этом словаре будет храниться дата отправленного пользователем письса
# если он уже играл сегодня, то ему не придет сообщение с приглашением в 12:00
# если не играл, то придет
flags = {}


# узнаем имя пользователя и заносим его в базу данных
# затем переходим к функции, с помощью которой узнаем режим игры
def get_name(message):
    name = message.text
    # ключ - id чата, значение - имя пользователя
    db_names[message.chat.id] = name
    bot.send_message(message.from_user.id, emoji.emojize(f'Хорошо, {name}! Отправьте любое сообщение,'
                                                         f' когда будете готовы к игре :writing_hand: !'))
    # если пользователь отправлял сегодня боту сообщения, то добавляем сегодняшнюю дату в словарь
    if str(tconv(message.date)) == str(date.today()):
        flags[message.chat.id] = str(date.today())
    bot.register_next_step_handler(message, get_mode)


# узнаем режим, в который пользователь хочет поиграть
# добавляем кнопки с названием режимов: простой и сложный
# затем переходим у функции, которая обрабатывает ответ пользователя и выбирает слово
def get_mode(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    btn1 = types.KeyboardButton(text="Сложный")
    btn2 = types.KeyboardButton(text="Простой")
    markup.add(btn1, btn2)
    bot.send_message(message.chat.id, text=emoji.emojize('Супер! Выберите режим игры :dizzy: '), reply_markup=markup)
    # если пользователь отправлял сегодня боту сообщения, то добавляем сегодняшнюю дату в словарь
    if str(tconv(message.date)) == str(date.today()):
        flags[message.chat.id] = str(date.today())
    bot.register_next_step_handler(message, func)


# после игры спрашиваем пользователя, хочет ли он играть. если хочет - спрашиваем про режим, если не хочет
# бот прекращает работу до 12:00 следующего дня
def after_game(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    btn1 = types.KeyboardButton("Хочу играть!")
    btn2 = types.KeyboardButton("Мне на сегодня хватит;)")
    markup.add(btn1, btn2)
    bot.send_message(message.chat.id, text="Вы молодец! Хотите ли вы поиграть еще?", reply_markup=markup)
    bot.register_next_step_handler(message, func_1)


# смотрим, что пользователь ответил на вопрос, хочет ли он поиграть еще
# если хочет - спрашиваем про режим, если не хочет - до 12:00 следующего дня
def func_1(message):
    if message.text == "Хочу играть!":
        bot.send_message(message.from_user.id, f'Отправьте любое сообщение, когда будете готовы к игре!')
        bot.register_next_step_handler(message, get_mode)
    elif message.text == "Мне на сегодня хватит;)":
        bot.send_message(message.from_user.id, emoji.emojize(f'Спасибо, что поиграли сегодня!'
                                                             f' Возвращайтесь завтра :smiling_face_with_open_hands:'))


# в этих словарях будем хранить слова, которые были загаданы пользователю в двух режимах
words_pers_h = []
words_pers_e = []


# в этой функции смотрим,какой режим пользователь выбрал
# разные действия отличаются только базами данных (для простых и сложных слов)
@bot.message_handler(content_types=['text'])
def func(message):
    if message.text == "Сложный":
        # проверяем, верно ли введена длина слова
        if message.text in lengths_h:
            length = int(message.text)
            # составляем массив возможных слов нужной длины
            needed_words = []
            for w in words_h:
                if len(w) == int(message.text):
                    needed_words.append(w)
            # случайным образом выбираем соово
            word = random.choice(needed_words)
            # если слово уже было загаданно
            if word in words_pers_h:
                words_pers_l = []
                # проверяем, может, пользователь уже угадал все слова с такой длиной
                for w in words_pers_h:
                    if len(w) == length:
                        words_pers_l.append(w)
                if len(words_pers_l) == len(needed_words):
                    bot.send_message(message.from_user.id, emoji.emojize(f'Упс! Вы уже угадали все слова с таким '
                                                                         f'количеством букв! '
                                                                         f'Введите новую длину :winking_face:'))
                # если нет, то пока слово не станет новым, используем рандомный выбор слов
                else:
                    while word in words_pers_h:
                        word = random.choice(needed_words)
            # добавляем слово в массив, обновляем массив в базе данных
            words_pers_h.append(word)
            db_words_h[message.chat.id] = words_pers_h
            # ключ - id чата, значение - массив, где первый (нулевой) элемент - длина, второй (первый) - слово
            db[message.chat.id] = [length, word]
            bot.send_message(message.from_user.id, f'Супер! Игра началась!'
                                                   f' Введите любое слово из {message.text} букв.')
            bot.register_next_step_handler(message, game_h)
        # если пользователь ввел число в неверном формате, просим его ввести еще раз
        else:
            bot.send_message(message.from_user.id, 'Введите число от 5 до 20 - длину слова.')
            bot.register_next_step_handler(message, get_nof_h)
    # тот же самый код для простого режима
    if message.text == "Простой":
        # проверяем, верно ли введена длина слова
        if message.text in lengths_e:
            length = int(message.text)
            # создаем массив возможных слов нужной длины
            needed_words = []
            for w in words_e:
                if len(w) == int(message.text):
                    needed_words.append(w)
            # случайным образом выбираем слово
            word = random.choice(needed_words)
            # если слово уже было загаданно
            if word in words_pers_e:
                words_pers_l = []
                # проверяем, может, пользователь уже угадал все слова с такой длиной
                for w in words_pers_e:
                    if len(w) == length:
                        words_pers_l.append(w)
                if len(words_pers_l) == len(needed_words):
                    bot.send_message(message.from_user.id, emoji.emojize(f'Упс! Вы уже угадали все слова с таким '
                                                                         f'количеством букв! '
                                                                         f'Введите новую длину :winking_face:'))
                # если нет, то пока слово не станет новым, используем рандомный выбор слов
                else:
                    while word in words_pers_e:
                        word = random.choice(needed_words)
            # добавляем слово в массив, обновляем массив в базе данных
            words_pers_e.append(word)
            db_words_e[message.chat.id] = words_pers_e
            # ключ - id чата, значение - массив, где первый (нулевой) элемент - длина, второй (первый) - слово
            db[message.chat.id] = [length, word]
            bot.send_message(message.from_user.id, f'Супер! Игра началась!'
                                                   f' Введите любое слово из {message.text} букв.')
            bot.register_next_step_handler(message, game_e)
        # если пользователь ввел число в неверном формате, просим его ввести еще раз
        else:
            bot.send_message(message.from_user.id, 'Введите число от 3 до 16 - длину слова.')
            bot.register_next_step_handler(message, get_nof_e)


# эта функция запускается, когда было введено число букв в неверном формате
def get_nof_e(message):
    if message.text in lengths_e:
        length = int(message.text)
        needed_words = []
        for w in words_e:
            if len(w) == int(message.text):
                needed_words.append(w)
        word = random.choice(needed_words)
        # если слово уже было загаданно
        if word in words_pers_e:
            words_pers_l = []
            # проверяем, может, пользователь уже угадал все слова с такой длиной
            for w in words_pers_e:
                if len(w) == length:
                    words_pers_l.append(w)
            if len(words_pers_l) == len(needed_words):
                bot.send_message(message.from_user.id, emoji.emojize(f'Упс! Вы уже угадали все слова с таким '
                                                                     f'количеством букв! '
                                                                     f'Введите новую длину :winking_face:'))
            # если нет, то пока слово не станет новым, используем рандомный выбор слов
            else:
                while word in words_pers_e:
                    word = random.choice(needed_words)
        # добавляем слово в массив, обновляем массив в базе данных
        words_pers_e.append(word)
        db_words_e[message.chat.id] = words_pers_e
        # ключ - id чата, значение - массив, где первый (нулевой) элемент - длина, второй (первый) - слово
        db[message.chat.id] = [length, word]
        bot.send_message(message.from_user.id, f'Супер! Игра началась!'
                                               f' Введите любое слово из {message.text} букв.')
        bot.register_next_step_handler(message, game_e)
    # если пользователь ввел число в неверном формате, просим его ввести еще раз
    else:
        bot.send_message(message.from_user.id, 'Введите число от 3 до 16 - длину слова.')
        bot.register_next_step_handler(message, get_nof_e)


# эта функция запускается, когда число букв было введено в неверном формате
def get_nof_h(message):
    if message.text in lengths_h:
        length = int(message.text)
        needed_words = []
        for w in words_h:
            if len(w) == int(message.text):
                needed_words.append(w)
        word = random.choice(needed_words)
        # если слово уже было загаданно
        if word in words_pers_h:
            words_pers_l = []
            # проверяем, может, пользователь уже угадал все слова с такой длиной
            for w in words_pers_h:
                if len(w) == length:
                    words_pers_l.append(w)
            if len(words_pers_l) == len(needed_words):
                bot.send_message(message.from_user.id, emoji.emojize(f'Упс! :grimacing_face: Вы уже угадали '
                                                                     f'все слова с таким '
                                                                     f'количеством букв! '
                                                                     f'Введите новую длину :winking_face:'))
            # если нет, то пока слово не станет новым, используем рандомный выбор слов
            else:
                while word in words_pers_h:
                    word = random.choice(needed_words)
        # добавляем слово в массив, обновляем массив в базе данных
        words_pers_h.append(word)
        db_words_h[message.chat.id] = words_pers_h
        # ключ - id чата, значение - массив, где первый (нулевой) элемент - длина, второй (первый) - слово
        db[message.chat.id] = [length, word]
        bot.send_message(message.from_user.id, f'Супер! Игра началась!'
                                               f' Введите любое слово из {message.text} букв.')
        bot.register_next_step_handler(message, game_h)
    # если пользователь ввел число в неверном формате, просим его ввести еще раз
    else:
        bot.send_message(message.from_user.id, 'Введите число от 5 до 20 - длину слова.')
        bot.register_next_step_handler(message, get_nof_h)


# код игры простого режима
def game_e(message):
    guess = message.text
    guess = guess.lower()
    # проверям, слово из скольких букв ввел пользователь, если неверный формат - выводим сообщение и снова запускаем
    # функцию
    if len(guess) != db[message.chat.id][0]:
        bot.send_message(message.from_user.id, emoji.emojize(f'Вы ввели слово в неправильном формате! '
                                                             f'Оно должно состоять из'
                                                             f' {db[message.chat.id][0]} букв!'
                                                             f' Попробуйте еще раз :winking_face: .'))
        bot.register_next_step_handler(message, game_e)
    else:
        # если пользователь угадал, то игра заканчивается и мы переходим к фукнции after_game
        if guess == db[message.chat.id][1]:
            bot.send_message(message.from_user.id, emoji.emojize('Вы угадали! :heart_on_fire:'))
            bot.send_message(message.from_user.id, emoji.emojize('Напишите, понравилось ли вам.'))
            bot.register_next_step_handler(message, after_game)
        # если пользователь не угадал, то присваиваем каждой букве эмодзи
        else:
            # в yellow_letters попадают те буквы, которые есть в слове, но в догадке пользователя стоят не на том месте
            yellow_letters = []
            for j in range(db[message.chat.id][0]):
                for k in range(db[message.chat.id][0]):
                    if guess[j] == db[message.chat.id][1][k]:
                        if j != k:
                            yellow_letters.append(guess[j])
            # выводим слово побуквенно
            for i in range(len(guess)):
                # если позиция буква в догадке и загадонном слове совпадает, то это зеленая буква
                if guess[i] == db[message.chat.id][1][i]:
                    bot.send_message(message.from_user.id, emoji.emojize(f'{guess[i]} - :green_square:'))
                # если буква есть в yellow_letters, то она желтая
                elif guess[i] in yellow_letters:
                    bot.send_message(message.from_user.id, emoji.emojize(f'{guess[i]} - :yellow_square:'))
                # если буквы нет в слове (иначе), то она красная
                else:
                    bot.send_message(message.from_user.id, emoji.emojize(f'{guess[i]} - :red_square:'))
            # играем, пока пользователь не угадает загаданное слово
            bot.register_next_step_handler(message, game_e)


# код для сложного режима игры
def game_h(message):
    guess = message.text
    guess = guess.lower()
    # проверям, слово из скольких букв ввел пользователь, если неверный формат, снова запускаем функцию
    if len(guess) != db[message.chat.id][0]:
        bot.send_message(message.from_user.id, emoji.emojize(f'Вы ввели слово в неправильном формате! '
                                                             f'Оно должно состоять из'
                                                             f' {db[message.chat.id][0]} букв! '
                                                             f'Попробуйте еще раз :winking_face: .'))
        bot.register_next_step_handler(message, game_h)
    else:
        # если пользователь угадал, то игра заканчивается
        if guess == db[message.chat.id][1]:
            bot.send_message(message.from_user.id, emoji.emojize('Вы угадали! :heart_on_fire:'))
            bot.send_message(message.from_user.id, emoji.emojize('Напишите, понравилось ли вам.'))
            bot.register_next_step_handler(message, after_game)
        # если пользователь не угадал, то присваиваем каждой букве эмодзи
        else:
            # в yellow_letters попадают те буквы, которые есть в слове, но в догадке пользователя стоят не на том месте
            yellow_letters = []
            for j in range(db[message.chat.id][0]):
                for k in range(db[message.chat.id][0]):
                    if guess[j] == db[message.chat.id][1][k]:
                        if j != k:
                            yellow_letters.append(guess[j])
            # выводим слово побуквенно
            for i in range(len(guess)):
                # если позиция буква в догадке и загадонном слове совпадает, то это зеленая буква
                if guess[i] == db[message.chat.id][1][i]:
                    bot.send_message(message.from_user.id, emoji.emojize(f'{guess[i]} - :green_square:'))
                # если буква есть в yellow_letters, то она желтая
                elif guess[i] in yellow_letters:
                    bot.send_message(message.from_user.id, emoji.emojize(f'{guess[i]} - :yellow_square:'))
                # если буквы нет в слове (иначе), то она красная
                else:
                    bot.send_message(message.from_user.id, emoji.emojize(f'{guess[i]} - :red_square:'))
            # играем, пока пользователь не угадает загаданное слово
            bot.register_next_step_handler(message, game_h)


# проверяем, нужное ли сейчас время, чтобы отправить ежедневную рассылку
def schedule_checker():
    while True:
        schedule.run_pending()
        time.sleep(1)


# в нужное время отправляем каждому пользователю из базы данных следующее сообщение, переходим к функции
# get_mode, чтобы узнать режим игры
def function_to_run():
    for ids in db_names:
        # если пользователя нет в словаре с датами, то отправляем ему сообщение с приглашением в игру
        if ids not in flags.keys():
            message = bot.send_message(ids, emoji.emojize(f'Привет, {db_names[ids]}!:smiling_face_with_open_hands: '
                                                          f'Давайте поиграем! Напишите любое сообщение, когда будете '
                                                          f'готовы! :writing_hand: '))
            bot.register_next_step_handler(message, get_mode)
        # если пользователь есть в словаре с датами, но дата там не сегодняшняя, отправляем приглашие в игру
        else:
            if flags[ids] != str(date.today()):
                message = bot.send_message(ids, emoji.emojize(f'Привет, {db_names[ids]}!:smiling_face_with_open_hands: '
                                                              f'Давайте поиграем! Напишите любое сообщение, когда '
                                                              f'будете '
                                                              f'готовы! :writing_hand: '))
                bot.register_next_step_handler(message, get_mode)


# определяем время рассылки - 12:00
if __name__ == "__main__":
    schedule.every().day.at("12:00").do(function_to_run)
    Thread(target=schedule_checker).start()

# чтобы бот работал всегда
telebot.apihelper.RETRY_ON_ERROR = True
bot.infinity_polling()
