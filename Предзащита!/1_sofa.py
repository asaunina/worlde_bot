import telebot
import emoji

with open('russian_nouns.txt', 'r', encoding='utf-8') as file:
    words_o = file.read()

words_o = words_o.split('\n')

bot = telebot.TeleBot('6218513407:AAFQemNN2Jckj35h3eYHuyE4OSWm95QWwNo')


@bot.message_handler(content_types=['text'])
def start(message):
    if message.text == '/start':
        bot.send_message(message.from_user.id, emoji.emojize('Привет! :smiling_face_with_open_hands: '
                                                             'Твоя задача - угадать существительное русского языка.'
                                                             ' Я буду присылать тебе новое слово каждый день!'))
    bot.send_message(message.from_user.id, 'Как к тебе можно обращаться? Напиши свое имя')
    bot.register_next_step_handler(message, get_name)


def get_name(message):
    name = message.text
    with open('chatids.txt', 'a+') as chatids:
        print(message.chat.id, name, file=chatids)
    bot.send_message(message.from_user.id, f'Хорошо, {name}! Начнем игру - напиши любое число от 2 до 24 - длину слова,'
                                           f' которое хочешь угадать.')
