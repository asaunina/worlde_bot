import telebot
import random
import emoji
import schedule
import time
from threading import Thread


# создаем массив из существительных, которые будем загадывать пользовател.
with open('russian_nouns.txt', 'r', encoding='utf-8') as file:
    words_o = file.read()
words_o = words_o.split('\n')

# создаем бота
bot = telebot.TeleBot('6218513407:AAFQemNN2Jckj35h3eYHuyE4OSWm95QWwNo')

# набор возможных длин слова, которые есть в нашем списке
lengths_o = ['2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14',
             '15', '16', '17', '18', '19', '20', '21', '22', '23', '24']

# создаем два словаря - базы данных
# в db храним длину слова и слово на каждый день на каждого пользователя
db = {}
# в db_names храним имя каждого пользователя
db_names = {}


# пишем функции для бота (он принимает только текстовые сообщения)
@bot.message_handler(content_types=['text'])
# когда пользователь запускает бота, он присылает ему приветствие, обозначения в игре и спрашивает имя, после этого
# запускается функци get_name
def start(message):
    if message.text == '/start':
        bot.send_message(message.from_user.id, emoji.emojize('Привет! :smiling_face_with_open_hands: '
                                                             'Твоя задача - угадать существительное русского языка.'
                                                             ' Я буду присылать тебе новое слово каждый день!'))
        bot.send_message(message.from_user.id, emoji.emojize('Вот обозначения в нашей игре:\n'
                                                             f':green_square: - буква есть в слове и стоит на '
                                                             f'правильном '
                                                             f'месте \n:yellow_square: - буква есть в слове, но стоит'
                                                             f' не на правильно месте \n:red_square: - буквы нет '
                                                             f'в слове'))
        bot.send_message(message.from_user.id, 'Как к тебе можно обращаться? Напиши свое имя')
        bot.register_next_step_handler(message, get_name)


# узнаем имя пользователя и заносим его в базу данных
# после этого спрашиваем длину слова, которое пользователь хочет угадать и переходим к функции get_nof
def get_name(message):
    name = message.text
    # ключ - id чата, значение - имя пользователя
    db_names[message.chat.id] = name
    bot.send_message(message.from_user.id, f'Хорошо, {name}! Начнем игру - напиши любое число от 2 до 24 - длину слова,'
                                           f' которое хочешь угадать.')
    bot.register_next_step_handler(message, get_nof)


# узнаем длину слова и выбираем случайное слово такое длины из списка, заносим в базу данных и переходим к игре
def get_nof(message):
    if message.text in lengths_o:
        length = int(message.text)
        needed_words = []
        for w in words_o:
            if len(w) == int(message.text):
                needed_words.append(w)
        word = random.choice(needed_words)
        # ключ - id чата, значение - массив, где первый (нулевой) элемент - длина, второй (первый) - слово
        db[message.chat.id] = [length, word]
        bot.send_message(message.from_user.id, f'Супер! Игра началась!'
                                               f' Введи любое слово из {message.text} букв.')
        bot.register_next_step_handler(message, game)
    # если пользователь ввел число в неверном формате, просим его ввести еще раз
    else:
        bot.send_message(message.from_user.id, 'Введите число от 2 до 24 - длину слова.')
        bot.register_next_step_handler(message, get_nof)


# после игры говорим пользователю, что на сегодня игра закончилась и нужно возвращаться завтра
def after_game(message):
    bot.send_message(message.from_user.id, emoji.emojize(f'Вы уже играли сегодня. Загаданное слово было -'
                                                         f' {db[message.chat.id][1]}.'
                                                         f' Возвращайтесь завтра :smiling_face_with_open_hands:'))


# код игры, когда пользователь угадал слово, переходим к after_game
def game(message):
    guess = message.text
    guess = guess.lower()
    # проверям, слово из скольких букв ввел пользователь
    if len(guess) != db[message.chat.id][0]:
        bot.send_message(message.from_user.id, f'Вы ввели слово в неправильном формате! Оно должно состоять из'
                                               f' {db[message.chat.id][0]} букв! Попробуйте еще раз.')
        bot.register_next_step_handler(message, game)
    else:
        # если пользователь угадал, то игра заканчивается
        if guess == db[message.chat.id][1]:
            bot.send_message(message.from_user.id, emoji.emojize('Вы угадали! :heart_on_fire: До встречи завтра!'))
            bot.register_next_step_handler(message, after_game)
        # если пользователь не угадал, то присваиваем каждой букве эмодзи
        else:
            # в yellow_letters попадают те буквы, которые есть в слове, но в догадке пользователя стоят не на том месте
            yellow_letters = []
            for j in range(db[message.chat.id][0]):
                for k in range(db[message.chat.id][0]):
                    if guess[j] == db[message.chat.id][1][k]:
                        if j != k:
                            yellow_letters.append(guess[j])
            # выводим слово побуквенно
            for i in range(len(guess)):
                # если позиция буква в догадке и загадонном слове совпадает, то это зеленая буква
                if guess[i] == db[message.chat.id][1][i]:
                    bot.send_message(message.from_user.id, emoji.emojize(f'{guess[i]} - :green_square:'))
                # если буква есть в yellow_letters, то она желтая
                elif guess[i] in yellow_letters:
                    bot.send_message(message.from_user.id, emoji.emojize(f'{guess[i]} - :yellow_square:'))
                # если буквы нет в слове (иначе), то она красная
                else:
                    bot.send_message(message.from_user.id, emoji.emojize(f'{guess[i]} - :red_square:'))
            # играем, пока пользователь не угадает загаданное слово
            bot.register_next_step_handler(message, game)


# проверяем, нужное ли сейчас время, чтобы отправить ежедневную рассылку
def schedule_checker():
    while True:
        schedule.run_pending()
        time.sleep(1)


# в нужное время отправляем каждому пользователю из базы данных следующее сообщение, переходим к функции
# get_nof, чтобы узнать количество букв
def function_to_run():
    for ids in db_names:
        message = bot.send_message(ids, emoji.emojize(f'Привет, {db_names[ids]}!:smiling_face_with_open_hands: '
                                                      f'Давай поиграем! '
                                                      f'Слово из скольких букв ты хочешь сегодня '
                                                      f'угадать? Напиши число от 2 до 24.'))
        bot.register_next_step_handler(message, get_nof)


# определяем время рассылки - 12:00
if __name__ == "__main__":
    schedule.every().day.at("12:00").do(function_to_run)
    Thread(target=schedule_checker).start()

# чтобы бот работал всегда
telebot.apihelper.RETRY_ON_ERROR = True
bot.infinity_polling()
